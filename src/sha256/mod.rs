use hkdf::{Hkdf, InvalidLength};
use sha3::{Digest, Sha3_256};

use super::utils::EMPTY_BYTES;

pub fn hash(data: &[u8]) -> Vec<u8> {
    let mut hasher = Sha3_256::new();
    hasher.update(data);

    (&hasher.finalize()).to_vec()
}

pub fn hkdf(password: &[u8]) -> Result<[u8; 32], InvalidLength> {
    let h = Hkdf::<Sha3_256>::new(None, password);

    let mut out = [0u8; 32];

    match h.expand(&EMPTY_BYTES, &mut out) {
        Ok(_) => Ok(out),
        Err(e) => Err(e),
    }
}

#[cfg(test)]
mod tests {
    use super::hash;
    use crate::utils::base64;

    const TEST_DATA: &[u8] = "hello world".as_bytes();
    const HASH: &str = "ZEvMflZDcwQJmarInnYi88px+6HZcv2Uoxw7+/JOOTg=";

    #[test]
    fn sha256_hash() -> Result<(), String> {
        let test_hash = match base64::decode(HASH) {
            Ok(h) => h,
            Err(e) => {
                println!("Error: {:?}", e.to_string());
                return Err(e.to_string());
            }
        };

        let h = hash(TEST_DATA);

        assert_eq!(h, test_hash);

        Ok(())
    }
}

pub mod sync;
pub mod utils;

pub use chacha20poly1305;

#[cfg(test)]
mod tests {
    use crate::chacha20::sync::{decrypt, encrypt};
    use async_std;

    const TEST_DATA: &str = "hello world";
    const TEST_KEY: &str = "b94d27b9934d3e08a52e52d7da7dabfa";

    #[async_std::test]
    async fn sync() -> Result<(), String> {
        let enc = match encrypt(TEST_DATA.as_bytes(), TEST_KEY.as_bytes()).await {
            Ok(e) => e,
            Err(e) => {
                println!("Error: {:?}", e.to_string());
                return Err(e.to_string());
            }
        };

        let dec = match decrypt(&enc[..], TEST_KEY.as_bytes()).await {
            Ok(d) => d,
            Err(e) => {
                println!("Error: {:?}", e.to_string());
                return Err(e.to_string());
            }        };

        let s = match std::str::from_utf8(&dec[..]) {
            Ok(v) => v,
            Err(e) => {
                println!("Error: {:?}", e.to_string());
                return Err(e.to_string());
            }        };

        assert_eq!(TEST_DATA, s);

        Ok(())
    }
}

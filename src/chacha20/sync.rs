use chacha20poly1305::{aead::KeyInit, AeadInPlace, Tag, XChaCha20Poly1305, XNonce};

use crate::utils::EMPTY_BYTES;

use super::utils::{generate_nonce, CHACHA_NONCE_LENGTH, CHACHA_NONCE_PLUS_TAG_LENGTH};

pub async fn encrypt(data: &[u8], key: &[u8]) -> Result<Vec<u8>, String> {
    if key.len() > 32 {
        return Err(format!("Key size is invalid (len={} != 32)", key.len()));
    }

    let aead = XChaCha20Poly1305::new(key.as_ref().into());
    let nonce = generate_nonce();

    let mut out = Vec::with_capacity(data.len());
    out.extend(data);

    let enc = match aead.encrypt_in_place_detached(&nonce, &EMPTY_BYTES, &mut out) {
        Ok(tag) => {
            let mut output = Vec::with_capacity(CHACHA_NONCE_PLUS_TAG_LENGTH + data.len());
            output.extend(nonce);
            output.extend(tag);
            output.extend(out);

            output
        }
        Err(e) => return Err(e.to_string()),
    };

    Ok(enc)
}

pub async fn decrypt(data: &[u8], key: &[u8]) -> Result<Vec<u8>, String> {
    if key.len() > 32 {
        return Err(format!("Key size is invalid (len={} != 32)", key.len()));
    }

    if data.len() < CHACHA_NONCE_PLUS_TAG_LENGTH {
        return Err(format!("Invalid data size (len={} != {})", data.len(), CHACHA_NONCE_PLUS_TAG_LENGTH));
    }

    let aead = XChaCha20Poly1305::new(key.as_ref().into());

    let nonce = XNonce::from_slice(&data[..CHACHA_NONCE_LENGTH]);
    let tag = Tag::from_slice(&data[CHACHA_NONCE_LENGTH..CHACHA_NONCE_PLUS_TAG_LENGTH]);

    let mut out = Vec::with_capacity(data.len() - CHACHA_NONCE_PLUS_TAG_LENGTH);
    out.extend(&data[CHACHA_NONCE_PLUS_TAG_LENGTH..]);

    match aead.decrypt_in_place_detached(nonce, &EMPTY_BYTES, &mut out, tag) {
        Ok(_) => Ok(out),
        Err(e) => Err(e.to_string()),
    }
}

use chacha20poly1305::{aead::OsRng, XNonce, Key};
use rand::RngCore;

pub const CHACHA_KEY_LENGTH: usize = 32;
pub const CHACHA_NONCE_LENGTH: usize = 24;
pub const CHACHA_STREAM_NONCE_LENGTH: usize = 19;
pub const CHACHA_TAG_LENGTH: usize = 16;

pub const CHACHA_NONCE_PLUS_TAG_LENGTH: usize = CHACHA_NONCE_LENGTH + CHACHA_TAG_LENGTH;

pub type XChaCha20Key = [u8; CHACHA_KEY_LENGTH];

pub fn generate_nonce() -> XNonce {
    let mut buf = [0u8; CHACHA_NONCE_LENGTH];
    OsRng.fill_bytes(&mut buf);
    *XNonce::from_slice(&buf)
}

pub fn generate_key() -> Key {
    let mut buf = [0u8; CHACHA_KEY_LENGTH];
    OsRng.fill_bytes(&mut buf);
    *Key::from_slice(&buf)
}
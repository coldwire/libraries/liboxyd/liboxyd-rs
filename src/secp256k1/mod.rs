pub mod ecdsa;
pub mod ecies;
pub mod keys;

pub use libsecp256k1;

#[cfg(test)]
mod tests {
    use crate::secp256k1::keys::Key;

    use super::{ecdsa, ecies, keys};
    use async_std;
    use libsecp256k1::PublicKey;

    const TEST_DATA: &[u8] = "hello world".as_bytes();

    #[async_std::test]
    async fn ecies() -> Result<(), String> {
        let (sk, pk) = keys::generate_keypair();

        let enc = match ecies::crypto::encrypt(pk, TEST_DATA).await {
            Ok(encrypted) => encrypted,
            Err(e) => {
                println!("Error: {:?}", e.to_string());
                return Err(e.to_string());
            }
        };

        let dec = match ecies::crypto::decrypt(sk, &enc).await {
            Ok(decypted) => decypted,
            Err(e) => {
                println!("Error: {:?}", e.to_string());
                return Err(e.to_string());
            }
        };

        assert_eq!(dec, TEST_DATA);

        Ok(())
    }

    #[test]
    fn ecdsa_signature() -> Result<(), String> {
        let (sk, pk) = keys::generate_keypair();

        let signature = match ecdsa::sign(sk, TEST_DATA) {
            Ok(res) => res,
            Err(e) => {
                println!("Error: {:?}", e.to_string());
                return Err(e.to_string());
            }
        };

        let is_valid = match ecdsa::verify(pk, signature, TEST_DATA) {
            Ok(valid) => valid,
            Err(e) => {
                println!("Error: {:?}", e.to_string());
                return Err(e.to_string());
            }
        };

        assert!(is_valid, "signature is valid ? {}", is_valid);

        Ok(())
    }

    #[test]
    fn ecdsa_recover_public_key() -> Result<(), String> {
        let (sk, pk) = keys::generate_keypair();

        let signature = match ecdsa::sign(sk, TEST_DATA) {
            Ok(res) => res,
            Err(e) => {
                println!("Error: {:?}", e.to_string());
                return Err(e.to_string());
            }
        };

        let public_key = match PublicKey::from_base64(pk) {
            Ok(p) => p,
            Err(e) => {
                println!("Error: {:?}", e.to_string());
                return Err(e.to_string());
            }
        };

        let recovered_public_key = match ecdsa::recover_public_key(signature, TEST_DATA) {
            Ok(r) => r,
            Err(e) => {
                println!("Error: {:?}", e.to_string());
                return Err(e.to_string());
            }
        };

        assert_eq!(
            recovered_public_key, public_key,
            "recovered publi key is good !"
        );

        Ok(())
    }
}

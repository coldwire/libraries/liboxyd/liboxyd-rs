use bip0039::Mnemonic;
use rand::{thread_rng};
use libsecp256k1::{PublicKey, SecretKey};

pub(crate) trait Key<T> {
    fn from_base64(key: String) -> Result<T, String>;
    fn to_base64(&self) -> String;
}

impl Key<PublicKey> for PublicKey {
    fn from_base64(pk: String) -> Result<PublicKey, String> {
        let pk = match base64::decode(pk) {
            Ok(k) => k,
            Err(e) => return Err(e.to_string()),
        };

        match PublicKey::parse_slice(&pk, None) {
            Ok(k) => Ok(k),
            Err(e) => return Err(e.to_string()),
        }
    }

    fn to_base64(&self) -> String {
        let pk = &self.serialize();
        base64::encode(pk)
    }
}

impl Key<SecretKey> for SecretKey {
    fn from_base64(sk: String) -> Result<SecretKey, String> {
        let sk = match base64::decode(sk) {
            Ok(k) => k,
            Err(e) => return Err(e.to_string()),
        };

        match SecretKey::parse_slice(&sk) {
            Ok(k) => Ok(k),
            Err(e) => return Err(e.to_string()),
        }
    }

    fn to_base64(&self) -> String {
        let sk = &self.serialize();
        base64::encode(sk)
    }
}

pub fn generate_keypair() -> (String, String) {
    let sk = SecretKey::random(&mut thread_rng());
    let pk = PublicKey::from_secret_key(&sk);

    (sk.to_base64(), pk.to_base64())
}

// Export to Mnemonic phrase
pub fn to_phrase(sk: String) -> Result<String, String> {
    let sk = match SecretKey::from_base64(sk).map(|v| v) {
        Ok(sk) => sk,
        Err(e) => return Err(e.to_string())
    };

    match Mnemonic::from_entropy(sk.serialize()) {
        Ok(res) => Ok(res.to_string()),
        Err(err) => Err(err.to_string()),
    }
}

// Import from mnemonic phrase
pub fn from_phrase(phrase: String) -> Result<String, String> {
    let sk = match Mnemonic::from_phrase(phrase) {
        Ok(res) => res.into_entropy(),
        Err(err) => return Err(err.to_string()),
    };

    let sk = match SecretKey::parse_slice(&sk) {
        Ok(s) => s,
        Err(err) => return Err(err.to_string()),
    };

    let sk = SecretKey::to_base64(&sk);

    Ok(sk)
}

pub fn pk_from_sk(sk: String) -> Result<String, String> {
    let sk = match SecretKey::from_base64(sk).map(|v| v) {
        Ok(sk) => sk,
        Err(e) => return Err(e.to_string())
    };

    let pk = PublicKey::from_secret_key(&sk);

    Ok(pk.to_base64())
}

use libsecp256k1::{util::FULL_PUBLIC_KEY_SIZE, PublicKey, SecretKey};

use crate::{chacha20::utils::XChaCha20Key, secp256k1::keys::Key, sha256};

/// Calculate a shared XChaCha20 key of our secret key and peer's public key by hkdf
pub fn encapsulate(sk: String, peer_pk: String) -> Result<XChaCha20Key, String> {
    let sk = match SecretKey::from_base64(sk) {
        Ok(sk) => sk,
        Err(e) => return Err(e.to_string())
    };

    let peer_pk = match PublicKey::from_base64(peer_pk) {
        Ok(pk) => pk,
        Err(e) => return Err(e.to_string()),
    };

    let mut shared_point = peer_pk;
    match shared_point.tweak_mul_assign(&sk) {
        Ok(_) => (),
        Err(e) => return Err(e.to_string())
    };

    let mut master = Vec::with_capacity(FULL_PUBLIC_KEY_SIZE * 2);
    master.extend(PublicKey::from_secret_key(&sk).serialize().iter());
    master.extend(shared_point.serialize().iter());

    match sha256::hkdf(master.as_slice()) {
        Ok(out) => Ok(out),
        Err(e) => return Err(e.to_string()),
    }
}

/// Calculate a shared XChaCha20 key of our public key and peer's secret key by hkdf
pub fn decapsulate(pk: String, peer_sk: String) -> Result<XChaCha20Key, String> {
    let pk = match PublicKey::from_base64(pk) {
        Ok(pk) => pk,
        Err(e) => return Err(e.to_string()),
    };

    let peer_sk = match SecretKey::from_base64(peer_sk) {
        Ok(sk) => sk,
        Err(e) => return Err(e.to_string())
    };

    let mut shared_point = pk;
    match shared_point.tweak_mul_assign(&peer_sk) {
        Ok(_) => (),
        Err(e) => return Err(e.to_string())
    };

    let mut master = Vec::with_capacity(FULL_PUBLIC_KEY_SIZE * 2);
    master.extend(pk.serialize().iter());
    master.extend(shared_point.serialize().iter());

    match sha256::hkdf(master.as_slice()) {
        Ok(out) => Ok(out),
        Err(e) => return Err(e.to_string()),
    }
}

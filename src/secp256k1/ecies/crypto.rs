use crate::secp256k1::{
    ecies::utils::{decapsulate, encapsulate},
    keys::{generate_keypair, Key},
};

use crate::chacha20::sync::{decrypt as chacha20_decrypt, encrypt as chacha20_encrypt};
use libsecp256k1::{util::FULL_PUBLIC_KEY_SIZE, PublicKey};

pub async fn encrypt(receiver_pk: String, data: &[u8]) -> Result<Vec<u8>, String> {
    let (ephemeral_sk, ephemeral_pk) = generate_keypair();

    let key = match encapsulate(ephemeral_sk, receiver_pk) {
        Ok(k) => k,
        Err(e) => return Err(e.to_string()),
    };

    let enc = match chacha20_encrypt(data, &key.as_ref()).await {
        Ok(e) => e,
        Err(e) => return Err(e.to_string()),
    };

    let mut cipher_text = Vec::with_capacity(FULL_PUBLIC_KEY_SIZE + enc.len());

    let ephemeral_pk = match PublicKey::from_base64(ephemeral_pk) {
        Ok(pk) => pk,
        Err(e) => return Err(e.to_string()),
    };

    cipher_text.extend(ephemeral_pk.serialize().iter());
    cipher_text.extend(enc);

    Ok(cipher_text)
}

pub async fn decrypt(receiver_sk: String, data: &[u8]) -> Result<Vec<u8>, String> {
    if data.len() < FULL_PUBLIC_KEY_SIZE {
        return Err(format!(
            "Invalid data size (len={} != {})",
            data.len(),
            FULL_PUBLIC_KEY_SIZE
        ));
    }

    let ephemeral_pk = match PublicKey::parse_slice(&data[..FULL_PUBLIC_KEY_SIZE], None) {
        Ok(epk) => PublicKey::to_base64(&epk),
        Err(e) => return Err(e.to_string()),
    };
    let encrypted = &data[FULL_PUBLIC_KEY_SIZE..];

    let key = match decapsulate(ephemeral_pk, receiver_sk) {
        Ok(k) => k,
        Err(e) => return Err(e.to_string()),
    };

    let dec = match chacha20_decrypt(encrypted, &key).await {
        Ok(d) => d,
        Err(e) => return Err(e.to_string()),
    };

    Ok(dec)
}

use libsecp256k1::{
    recover as secp256k1_recover, sign as secp256k1_sign, util::SIGNATURE_SIZE,
    verify as secp256k1_verify, Message, PublicKey, RecoveryId, SecretKey, Signature,
};

use crate::sha256;

use super::keys::Key;

pub fn sign(sk: String, data: &[u8]) -> Result<String, String> {
    let sk = match SecretKey::from_base64(sk).map(|v| v) {
        Ok(sk) => sk,
        Err(e) => return Err(e.to_string()),
    };
    let h = sha256::hash(data);

    let message = match Message::parse_slice(&h) {
        Ok(m) => m,
        Err(e) => return Err(e.to_string()),
    };

    let (signature, recovery) = secp256k1_sign(&message, &sk);

    let mut full_signature = Vec::with_capacity(SIGNATURE_SIZE + 8);
    full_signature.extend(signature.serialize().iter());
    full_signature.append(&mut vec![recovery.serialize()]);

    Ok(base64::encode(full_signature))
}

pub fn verify(pk: String, signature: String, data: &[u8]) -> Result<bool, String> {
    let pk = match PublicKey::from_base64(pk) {
        Ok(pk) => pk,
        Err(e) => return Err(e.to_string()),
    };

    let full_signature = match base64::decode(signature) {
        Ok(s) => s,
        Err(e) => return Err(e.to_string()),
    };

    let signature = match Signature::parse_standard_slice(&full_signature[..SIGNATURE_SIZE]) {
        Ok(s) => s,
        Err(e) => return Err(e.to_string()),
    };

    let h = sha256::hash(data);

    let message = match Message::parse_slice(&h) {
        Ok(m) => m,
        Err(e) => return Err(e.to_string()),
    };

    let res = secp256k1_verify(&message, &signature, &pk);
    Ok(res)
}

pub fn recover_public_key(signature: String, data: &[u8]) -> Result<PublicKey, String> {
    let full_signature = match base64::decode(signature) {
        Ok(s) => s,
        Err(e) => return Err(e.to_string()),
    };


    let signature = match Signature::parse_standard_slice(&full_signature[..SIGNATURE_SIZE]) {
        Ok(s) => s,
        Err(e) => return Err(e.to_string()),
    };

    let recovery = match RecoveryId::parse(full_signature[SIGNATURE_SIZE..][0]) {
        Ok(r) => r,
        Err(e) => return Err(e.to_string()),
    };
    
    let h = sha256::hash(data);

    let message = match Message::parse_slice(&h) {
        Ok(m) => m,
        Err(e) => return Err(e.to_string()),
    };

    match secp256k1_recover(&message, &signature, &recovery) {
        Ok(pk) => Ok(pk),
        Err(e) => return Err(e.to_string()),
    } 
}
